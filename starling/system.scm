;;; Starling Game Engine
;;; Copyright © 2020 David Thompson <davet@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Starling.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; OS/window system related things.
;;
;;; Code:

(define-module (starling system)
  #:use-module (oop goops)
  #:use-module (sdl2)
  #:use-module (sdl2 video)
  #:export (elapsed-time
            current-window
            current-window-size))

(define %time-freq (exact->inexact (sdl-performance-frequency)))

(define (elapsed-time)
  "Return the current value of the system timer in seconds."
  (/ (sdl-performance-counter) %time-freq))

(define current-window (make-parameter #f))

(define (current-window-size)
  (window-size (current-window)))
